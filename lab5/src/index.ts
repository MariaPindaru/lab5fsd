import "reflect-metadata";
import { createConnection } from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import { Request, Response } from "express";
import { Routes } from "./routes";

import * as fileUpload from "express-fileupload";

const tf = require('@tensorflow/tfjs');
require('@tensorflow/tfjs-node');



createConnection().then(async connection => {

    // create express app
    const app = express();
    const cors = require('cors')

    app.use(cors({ origin: '*' }));

    require("dotenv").config();

    app.use(bodyParser.json());
    app.use(fileUpload());

    var router = express.Router();

    // register express routes from defined application routes
    Routes.forEach(route => {
        if (route.middlewares) {
            route.middlewares.forEach((middleware) => {
                (router as any)[route.method](route.route, middleware);
            });
        }

        (router as any)[route.method](
            route.route,
            (req: Request, res: Response, next: Function) => {
                const result = new (route.controller as any)()[route.action](req, res, next);
                if (result instanceof Promise) {
                    result.then((result) =>
                        result !== null && result !== undefined ? res.send(result) : undefined);
                } else if (result !== null && result !== undefined) {
                    res.json(result);
                }
            }
        );
    });

    app.use("/", router);

    // start express server
    app.listen(3001);

    console.log("Express server has started on port 3001. Open http://localhost:3001/users to see results");

}).catch(error => console.log(error));
