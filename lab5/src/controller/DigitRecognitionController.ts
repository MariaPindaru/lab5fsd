import { getRepository } from "typeorm";
import { NextFunction, Request, Response } from "express";

import { MnistData } from "../digit-recognition/data.js";
import { getModel, train, doPrediction } from "../digit-recognition/script.js";

import * as fileUpload from "express-fileupload";

const tf = require('@tensorflow/tfjs');
require('@tensorflow/tfjs-node');

async function toArrayBuffer(buf) {
    var ab = new ArrayBuffer(buf.length);
    var view = new Uint8Array(ab);
    for (var i = 0; i < buf.length; ++i) {
        view[i] = buf[i];
    }
    return ab;
}

export class DigitRecognitionController {

    async train(request: Request, response: Response, next: NextFunction) {
        try {
            const mnist_data = new MnistData();
            var model = getModel();

            await mnist_data.load();
            const train_results = await train(model, mnist_data);
            await model.save("file://./src/digit-recognition/model");

            return response.status(200).send(train_results);


        } catch (error) {
            console.log(error);
            return response.status(500).send(error);
        }
    }

    async evaluate(request: Request, response: Response, next: NextFunction) {
        try {

            if (!request.files || Object.keys(request.files).length === 0) {
                return response.status(400).send('No files were uploaded.');
            }

            let sampleFile = request.files.sampleFile;

            const model = await tf.loadLayersModel("file://./src/digit-recognition/model/model.json");

            const arrayBuffer = await toArrayBuffer(sampleFile.data);

            try{
            var prediction = await doPrediction(model, arrayBuffer);
            }catch(err){
                return response.status(400).send(err);
            }
            
            return response.status(200).send(
                {
                    "prediction": prediction[0]
                });

        } catch (error) {
            console.log(error);
            return response.status(500).send(JSON.stringify(error));
        }
    }

}