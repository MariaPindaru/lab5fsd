import { UserController } from "./controller/UserController";
import { verifyToken } from "./midllewares/VerifyToken";
import { verifyUserId } from "./midllewares/Validation";
import { DigitRecognitionController } from "./controller/DigitRecognitionController";

export const Routes = [{
  method: "get",
  route: "/users",
  controller: UserController,
  action: "all",
  middlewares: [verifyToken]
}, {
  method: "get",
  route: "/users/:id",
  controller: UserController,
  action: "one",
  middlewares: [verifyToken, verifyUserId]
}, {
  method: "post",
  route: "/users",
  controller: UserController,
  action: "save",
  middlewares: [verifyToken]
}, {
  method: "delete",
  route: "/users/:id",
  controller: UserController,
  action: "remove",
  middlewares: [verifyToken, verifyUserId]
},
{
  method: "put",
  route: "/users/:id",
  controller: UserController,
  action: "update",
  middlewares: [verifyToken, verifyUserId]
},
{
  method: "post",
  route: "/login",
  controller: UserController,
  action: "login"
},
{
  method: "put",
  route: "/register",
  controller: UserController,
  action: "register",
},

{
  method: "post",
  route: "/train",
  controller: DigitRecognitionController,
  action: "train",
},
{
  method: "post",
  route: "/evaluate",
  controller: DigitRecognitionController,
  action: "evaluate",
},

];