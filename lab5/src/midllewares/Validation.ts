import { NextFunction, Request, Response } from "express";
import { getRepository } from "typeorm";
import { User } from "../entity/User";

export async function verifyUserId(request: Request, response: Response, next: NextFunction) {
  try {
    let userRepository = getRepository(User);
    await userRepository.findOneOrFail(request.params.id);
  }
  catch (error) {
    return response.status(404).send(`Invalid user id`);
  }

  return next();
}
