import { NextFunction, Request, Response } from "express";

var jwt = require("jsonwebtoken");

export function verifyToken(req: Request, res: Response, next: NextFunction) {
  const authHeader = req.headers.authorization;

  if (authHeader) {
    const token = authHeader.split(' ')[1];

    jwt.verify(token, process.env.SECRET_KEY, (error, user) => {
      if (error) {
        return res.sendStatus(403);
      }
      req.user = user;
      next();
    });

  } else {
    res.sendStatus(401);
  }
}