const { Datastore } = require("@google-cloud/datastore");
const moment = require("moment");

const datastore = new Datastore({
  projectId: 'sturdy-ranger-334316',
  keyFilename: 'sturdy-ranger-334316-6c9938ec3c79.json'
});

const kindName = 'Schedule';

//  https://us-central1-sturdy-ranger-334316.cloudfunctions.net/createSchedule

exports.createSchedule = (req, res) => {

  res.set('Access-Control-Allow-Origin', '*');

  if (req.method === 'OPTIONS') {
    // Send response to OPTIONS requests
    res.set('Access-Control-Allow-Methods', 'POST');
    res.set('Access-Control-Allow-Headers', 'Content-Type');
    res.set('Access-Control-Max-Age', '3600');
    res.status(204).send('');
    return;
  }

  res.header('Content-Type','application/json'); 
 
  let start_date = req.body.start_date, end_date = req.body.end_date;

  if (!start_date || !end_date) {
    res.status(500).send(JSON.stringify({ error: "Missing start or end date" }));
    return;
  }

  if (!moment(start_date).isValid() || !moment(end_date).isValid()) {
    res.status(500).send(JSON.stringify({ error: "Invalid start or end date." }));
    return;
  }

  if (new Date(start_date) > new Date(end_date)) {
    res.status(500).send(JSON.stringify({ error: "Start date must not be after the end date" }));
    return;
  }

  datastore
    .save({
      key: datastore.key(kindName),
      data: {
        start_date: start_date,
        end_date: end_date
      },
    })
    .then((response) => {
      console.log(response);
      res.status(200).send(JSON.stringify({ start_date, end_date }));
    })
    .catch((err) => {
      res.status(500).send(JSON.stringify(err));
    });
};

// https://us-central1-sturdy-ranger-334316.cloudfunctions.net/getSchedules

exports.getSchedules = (req, res) => {
  const query = datastore.createQuery(kindName);
  // Set CORS headers for preflight requests
  // Allows GETs from any origin with the Content-Type header
  // and caches preflight response for 3600s
  res.set('Access-Control-Allow-Origin', '*');

  if (req.method === 'OPTIONS') {
    // Send response to OPTIONS requests
    res.set('Access-Control-Allow-Methods', 'GET');
    res.set('Access-Control-Allow-Headers', 'Content-Type');
    res.set('Access-Control-Max-Age', '3600');
    res.status(204).send('');
    return;
  }

  res.header('Content-Type', 'application/json');

  datastore
    .runQuery(query)
    .then((result) => {
      res.status(200).send(result[0]);
    })
    .catch((err) => {
      res.status(500).send(JSON.stringify(err));
    });
};
